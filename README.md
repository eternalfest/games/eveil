# Éveil

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/eveil.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/eveil.git
```
